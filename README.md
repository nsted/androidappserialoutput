# README #

### What is this repository for? ###

* The android app provides a simple tablet-based UI for controlling an Arduino (or similar) to be used in an interactive installation. A slideshow plays when no one is interacting.
* The screen is divided into a 2x2 grid of buttons. When buttons are pushed their corresponding value out is sent out the USB serial port, and the timer is reset.
When the timer expires the app fades to a slideshow. If the slideshow is tapped the app fades back to buttons and the timer is reset again.

* Version 1

### How do I get set up? ###

* Install Android SDK
* Install Cordova CLI
* Clone Repository, cd into "cordovarduinoApp"
* "cordova run" while connected to target device to upload.
* Use app pinning to put into kiosk mode (ie. prevent escape).
* Use Wakey or similar app, to prevent screen from going to sleep.
* Included Cordova plugins: Cordovarduino, Cordova-Plugin-Fullscreen

### Who do I talk to? ###

* nick stedman - nick@robotstack.com