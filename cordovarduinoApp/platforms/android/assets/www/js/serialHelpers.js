/***

Serial Related Helpers and Settings.

Detailed comments in "index.js"

***/


var serialHelpers = {};

(function(context) {

	open = false;
	var str = '';

	var errorCallback = function(message) {
	    alert('Error: ' + message);
	    console.log('Error: ' + message)
	};

	var successCallback = function(data){
	    // decode the received message
	    var view = new Uint8Array(data);
	    if(view.length >= 1) {
	        for(var i=0; i < view.length; i++) {
	            // if we received a \n, the message is complete, display it
	            if(view[i] == 10) {
	                // display the message
	                var value = parseInt(str);
	                console.log('Received: ' + str);
	                str = '';
	            }
	            // if not, concatenate with the begening of the message
	            else {
	                var temp_str = String.fromCharCode(view[i]);
	                var str_esc = escape(temp_str);
	                str += unescape(str_esc);
	            }
	        }
	    }
	};


	context.getOpen = function() {
		return open;
	};

	context.closePort = function() {
	    serial.close(
	        // if port is succesfuly close
	        function(successMessage) {
	            console.log('port closed!');
	            open = false;
	        },
	        // error opening the port
	        errorCallback
	    );
	};        

	context.openPort = function() {
	    // request permission first
	    serial.requestPermission(
	        {
	            // connect to Arduino Mega (Arduino SRL)
	            vid: '2A03', 				// (Used in Telus Installation)
	            // vid: '2341', 					// (Used in Testing)
	            pid: '0042'
	            // driver: 'usbser' // or any other
	        },
	        // if user grants permission
	        function(successMessage) {
	            // open serial port
	            serial.open(
	                {baudRate: 9600, sleepOnPause: false},
	                // if port is succesfuly opened
	                function(successMessage) {
	                    console.log('port opened!');                            
	                    open = true;
	                    // register the read callback
	                    serial.registerReadCallback(
	                        successCallback,
	                        // error attaching the callback
	                        errorCallback
	                    );
	                },
	                // error opening the port
	                errorCallback
	            );
	        },
	        // user does not grant permission
	        errorCallback
	    );
	};
})(serialHelpers);
