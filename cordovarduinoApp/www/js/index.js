/***

When buttons are pushed send their corresponding value out the serial port and reset the timer.
If the timer times out then fade to the screen overlay. 
If the screen overlay is tapped then fade back to buttons and reset the timer again.

Nick Stedman
Summer 2016
nick@robotstack.com

The MIT License (MIT)
Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

***/

console.log('load index.js');

var app = {
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        console.log('deviceready');

        // button related
        var one = document.getElementById('one');
        var two = document.getElementById('two');
        var three = document.getElementById('three');
        var four = document.getElementById('four');    

        var btnVid1 = document.getElementById("btn1Video");
        var btnVid2 = document.getElementById("btn2Video");
        var btnVid3 = document.getElementById("btn3Video");
        var btnVid4 = document.getElementById("btn4Video");           

        var slideVid1 = document.getElementById("slide1Video");    
        var slideVid2 = document.getElementById("slide2Video");            

        // timer related
        var timer;        
        const COUNTDOWN = 20000;


        // disable scrolling
        document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);

        // Full Screen related
        function successFunction() {
            console.log("Full Screen");
        }

        function errorFunction(error) {
            console.error(error);
        }

        AndroidFullScreen.immersiveMode(successFunction, errorFunction);

        // timer related
        function resetTimer() {
            clearTimeout(timer);
            timer = setTimeout(fadeInOverlay, COUNTDOWN);    
            console.log("timer reset: " + timer)
        };


        // button related
        // handle button taps 
        one.onclick = function() {
            console.log('button hit - one triggered');
            resetTimer();
            console.log("serialHelpers.getOpen(): " + serialHelpers.getOpen());
            if (serialHelpers.getOpen()) serial.write('1');
        };

        two.onclick = function() {
            console.log('button hit - two triggered');
            resetTimer();  
            console.log("serialHelpers.getOpen(): " + serialHelpers.getOpen());   
            if (serialHelpers.getOpen()) serial.write('2');
        };

        three.onclick = function() {
            console.log('button hit - three triggered');
            resetTimer();  
            console.log("serialHelpers.getOpen(): " + serialHelpers.getOpen());      
            if (serialHelpers.getOpen()) serial.write('3');
        };

        four.onclick = function() {
            console.log('button hit - four triggered');
            resetTimer();   
            console.log("serialHelpers.getOpen(): " + serialHelpers.getOpen());         
            if (serialHelpers.getOpen()) serial.write('4');
        };   


        // Timer events: Faders!!
        function fadeInOverlay() {
            console.log('timed out - five triggered');
            slideVid1.play();
            slideVid2.play();
            $('#idle-screen').fadeIn();  
            console.log("serialHelpers.getOpen(): " + serialHelpers.getOpen());
            if (serialHelpers.getOpen()) serial.write('5');
            // close port when transitioning to screen overlay
            serialHelpers.closePort();
            btnVid1.pause();
            btnVid2.pause();
            btnVid3.pause();
            btnVid4.pause();         
        };

        $('#idle-screen').click(function(){
            btnVid1.play();
            btnVid2.play();
            btnVid3.play();
            btnVid4.play();
            // open port when transitioning back to buttons
            serialHelpers.openPort();            
            $(this).fadeOut();
            slideVid1.pause();
            slideVid2.pause();
            resetTimer();
        });
    

        // start 'em up!
        // open the port 
        serialHelpers.openPort();

        // reset the main timer
        resetTimer();  
    }
};

app.initialize();